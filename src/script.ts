let counter: number = 0;

let counter_display: HTMLElement = document.getElementById('counter')!;
let plus: HTMLElement = document.getElementById('counter_plus')!;
let reset: HTMLElement = document.getElementById('counter_reset')!;
let minus: HTMLElement = document.getElementById('counter_minus')!;

const handlePlus = () => {
    counter += 1;
    updateCounter();
}

const handleReset = () => {
    counter = 0;
    updateCounter();
}

const handleMinus = () => {
    if(counter !== 0) {
        counter -= 1;
        updateCounter();
    } 
}

const updateCounter = () => {
    counter_display.innerHTML = String(counter);
}

plus.addEventListener('click', handlePlus);
reset.addEventListener('click', handleReset);
minus.addEventListener('click', handleMinus);
