"use strict";
let counter = 0;
let counter_display = document.getElementById('counter');
let plus = document.getElementById('counter_plus');
let reset = document.getElementById('counter_reset');
let minus = document.getElementById('counter_minus');
const handlePlus = () => {
    counter += 1;
    updateCounter();
};
const handleReset = () => {
    counter = 0;
    updateCounter();
};
const handleMinus = () => {
    if (counter !== 0) {
        counter -= 1;
        updateCounter();
    }
};
const updateCounter = () => {
    counter_display.innerHTML = String(counter);
};
plus.addEventListener('click', handlePlus);
reset.addEventListener('click', handleReset);
minus.addEventListener('click', handleMinus);
